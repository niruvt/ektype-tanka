आज्ञासंच:    EkType-tanka
लेखक:       निरंजन
आवृत्ती:      ०.२ (जुलै, २०२०)
माहिती:      एक-टाईप ह्या संस्थेचे काही उत्कृष्ट देवनागरी टंक पुरवणारा आज्ञासंच
दुवा:     	  https://gitlab.com/niranjanvikastambe/ektype-tanka
अडचणी:     https://gitlab.com/niranjanvikastambe/ektype-tanka/-/issues
परवाना:      लाटेक् प्रकल्प परवाना. आवृत्ती १.३सी किंवा त्यापुढील.
--------------------------------------------------------------------------
दुवे -
मुक्त - https://ektype.in/scripts/devanagari/mukta.html
बलू - https://ektype.in/scripts/devanagari/baloo2.html
मोदक - https://ektype.in/scripts/devanagari/modak.html
जैनी - https://ektype.in/scripts/devanagari/jaini.html
गोटू - https://ektype.in/font-family/gotu-font/gotu-3230.html
--------------------------------------------------------------------------
टंककार -
मुक्त - गिरीश दळवी, यशोदीप घोलप
बलू - सारंग कुलकर्णी
मोदक - सारंग कुलकर्णी, मैथिली शिंगरे
जैनी - गिरीश दळवी, मैथिली शिंगरे
गोटू - सारंग कुलकर्णी, कैलाश मालवीय
--------------------------------------------------------------------------
अधिक माहितीकरिता EkType-tanka.pdf ही बीजधारिका पाहा.
--------------------------------------------------------------------------
आज्ञासमुच्चय:    EkType-tanka
लेखक:          निरंजन
संस्करण: 	   ०.२ (जुलै, २०२०)
जानकारी:       एक-टाइप संस्था के कई उत्कृष्ट देवनागरी टंकों का संग्रह।
कड़ी:           https://gitlab.com/niranjanvikastambe/ektype-tanka
त्रुटि:           https://gitlab.com/niranjanvikastambe/ektype-tanka/-/issues
अनुज्ञप्ति:       लाटेक् प्रकल्प अनुज्ञप्ति. संस्करण १.३सी या अधिक.
--------------------------------------------------------------------------
कड़ियाँ -
मुक्त - https://ektype.in/scripts/devanagari/mukta.html
बलू - https://ektype.in/scripts/devanagari/baloo2.html
मोदक - https://ektype.in/scripts/devanagari/modak.html
जैनी - https://ektype.in/scripts/devanagari/jaini.html
गोटू - https://ektype.in/font-family/gotu-font/gotu-3230.html
--------------------------------------------------------------------------
टंककार -
मुक्त - गिरीश दळवी, यशोदीप घोलप
बलू - सारंग कुलकर्णी
मोदक - सारंग कुलकर्णी, मैथिली शिंगरे
जैनी - गिरीश दळवी, मैथिली शिंगरे
गोटू - सारंग कुलकर्णी, कैलाश मालवीय
--------------------------------------------------------------------------
अधिक जानकारी के लिए EkType-tanka.pdf यह बीज-धारिका देखिए.
--------------------------------------------------------------------------
Package:      EkType-tanka
Author:       Niranjan
Version:      0.2  (July, 2020)
Description:  Collection of some excellent Devanagari fonts by EkType.
Repository:   https://gitlab.com/niranjanvikastambe/ektype-tanka
Bug tracker:  https://gitlab.com/niranjanvikastambe/ektype-tanka/-/issues
License:      The LaTeX Project Public License v1.3c or later.
--------------------------------------------------------------------------
Links -
Mukta - https://ektype.in/scripts/devanagari/mukta.html
Baloo - https://ektype.in/scripts/devanagari/baloo2.html
Modak - https://ektype.in/scripts/devanagari/modak.html
Jaini - https://ektype.in/scripts/devanagari/jaini.html
Gotu - https://ektype.in/font-family/gotu-font/gotu-3230.html
--------------------------------------------------------------------------
Font authors -
Mukta - Girish Dalvi, Yashodeep Gholap
Baloo - Sarang Kulkarni
Modak - Sarang Kulkarni, Maithili Shingre
Jaini - Girish Dalvi, Maithili Shingre
Gotu - Sarang Kulkarni, Kailash Malviya
--------------------------------------------------------------------------
For more information see EkType-tanka.pdf.
--------------------------------------------------------------------------